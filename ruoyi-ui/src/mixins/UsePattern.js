import { ref } from 'vue'

export default function () {
    // 手机号校验
    const phonePattern = ref(/^1[3|4|5|6|7|8|9][0-9]\d{8}$/);
    // 身份证号校验
    const idCardPattern = ref(/^(([1-9][0-9]{5}(19|20)[0-9]{2}((0[1-9])|(1[0-2]))([0-2][1-9]|10|20|30|31)[0-9]{3}([0-9]|X|x))|([1-9][0-9]{5}[0-9]{2}((0[1-9])|(1[0-2]))([0-2][1-9]|10|20|30|31)[0-9]{3}))$/);

    return {
        phonePattern,
        idCardPattern
    }
}