import request from '@/utils/request'

// 查询人员信息列表
export function listPeople(query) {
  return request({
    url: '/system/people/list',
    method: 'get',
    params: query
  })
}

// 查询人员信息详细
export function getPeople(uuid) {
  return request({
    url: '/system/people/' + uuid,
    method: 'get'
  })
}

// 新增人员信息
export function addPeople(data) {
  return request({
    url: '/system/people',
    method: 'post',
    data: data
  })
}

// 修改人员信息
export function updatePeople(data) {
  return request({
    url: '/system/people',
    method: 'put',
    data: data
  })
}

// 删除人员信息
export function delPeople(uuid) {
  return request({
    url: '/system/people/' + uuid,
    method: 'delete'
  })
}
